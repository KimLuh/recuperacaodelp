Nome: Luana Agatha Dos Santos 
Turma: 3-52
  
                                            ENUNCIADO DA PROVA

1. As estruturas de programa��o das linguagens permitem criar l�gicas para o fluxo dos dados de nossos softwares. 
Assinale a alternativa que  cont�m uma estrutura de programa��o usada pela linguagem Object-Pascal que permitem 
iterar indefinidamente sobre um mesmo conjunto de instru��es:

ALTERNATIVA: 

D)While 

JUSTIFICATIVA: 

   Esta instru��o � usada quando n�o sabemos quantas vezes um determinado bloco de instru��es 
precisa ser repetido.


2. As estruturas de dados presentes nas linguagens de programa��o s�o necess�rias para que a aplica��o possa entender e 
armazenar corretamente os dados que pretendemos utilizar. Assinale a alternativa que cont�m, respectivamente, 
uma estrutura capaz de armazenar apenas n�meros inteiros positivos e negativamos e uma estrutura que armazena apenas
dois valores: 

ALTERNATIVA:

E)INTEGER E BOOLEAN

JUSTIFICATIVA:

   Porque integer armazena todos os n�meros inteiros sej� positivos ou negativos, e Boolean consiste
somente dois valores: True e False.


3. V�rias linguagens de programa��o permitem a cria��o de enumeradores. Este tipo de estrutura de dados permitem criar 
uma vari�vel que armazena um valor pr�-definido pelo pr�prio programador. Utilizando como refer�ncia a linguagem 
Object-Pascal, Assinale a alternativa que representa a cria��o de um enumerador para os dias de semana:

ALTERNATIVA:

E)type diaSemana=(seg,ter,qua,qui,sex);

JUSTIFICATIVA:

   � utilizado como uma lista de itens onde � definido os valores dele ser�o acessados por um nome 
e n�o por um n�mero.


4. Existe uma estrutura definida pelo Object-Pascal capaz de agrupar itens de dados de diferentes tipos (ao contr�rio
do array, que armazena v�rios itens de mesmo tipo). Assinale a alternativa que representa o nome desta estrutura:

ALTERNATIVA:

A)Record

JUSTIFICATIVA:

  Armazena diferentes tipos, como 'Pocked record' no qual todos s�o alinhados em bytes, pode construir em diferentes tipos.


5. O git � um sistema de controle de vers�es desenvolvido por Linus Torvalds e Junio Hamano que facilita o processo
de desenvolvimento de software ao ser usado para registrar o hist�rico de edi��es dos arquivos-fonte. Assinale a 
alternativa que n�o representa uma fun��o do Git:

ALTERNATIVA:

C)Impedir o acesso n�o autorizado aos arquivos fonte por meio da autentica��o de us�ario.

JUSTIFICATIVA:

   As outras alternativas s�o fun��es do git, como registrar o hist�rico de edi��es de arquivos em
uma pasta/projeto.


6. Para organizar a �rea de desenvolvimento, o Git implementa diversas �reas com diferentes caract�risticas dentro de 
um projeto. Dessa forma, o Git minimiza altera��es desastrosas que podem comprometer a integridade do c�digo. Assinale 
a alternativa que representa o nome da �rea do Git onde ficam armazenados os arquivos que est�o prontos para serem 
preservados permanentemente no reposit�rio local (por�m ainda n�o foram):

ALTERNATIVA:

D)Stage/Index

JUSTIFICATIVA:

 Utilizando o git commit salva uma copia do Stage. Onde fica armazenado antes de ser enviado ao remoto.


7.Ao realizar altera��es no c�digo, o Git n�o preserva automaticamente as altera��es efetuadas em seu reposit�rio.
Antes de mais nada, � necess�rio indicar ao versionador quais s�o as modifica��es que pretendemos preservar em uma 
nova vers�o. Assinale a alternativa que preserva APENAS as altera��es realizadas no arquivo stark.php (levando em 
considera��o que o arquivo do mesmo projeto Vingadores.php tamb�m possui altera��es):

ALTERNATIVA:

A)git add stark.php

JUSTIFICATIVA:

 Vai adicionar somente o arquivo stark.php, para preservar as atualiza��es deste arquivo.


8.Quando h� modifica��es listadas no Index do reposit�rio Git, podemos preservar permanentemente estas altera��es em
um processo chamado de commit. Assinale a alternativa que melhor descreve o comportamento do comando git commit:

ALTERNATIVA:

B)Cria-se um identificador �nico para o commit e as modifica��es s�o preservadas no reposit�rio local.

JUSTIFICATIVA:

 Criar o haver deste commit.


9. O reposit�rio remoto � uma defini��o do git para uma c�pia remota do reposit�rio local de um determinado projeto.
No entanto, estes dois tipos de reposit�rio podem conter diferen�as entre si, que necessitam da atualiza��o do programador.
Assinale a alternativa que representa, respectivamente, o comnaod utilizado para altualizar o reposit�rio local (com o 
conte�do remoto) e o comando utilizado para atualizar o reposit�rio remoto (com o conte�do local).

ALTERNATIVA:

A)GIT PULL E GIT PUSH

JUSTIFICATIVA:

 Ger�ncia os reposit�rios, atualizar com git pull e o git push ir� enviar com o conte�do para o reposit�rio.


10. Os gerenciadores de reposit�rio baseados em git (como o GitLab) s�o respons�veis por permitir que desenvolvedores
armazenam remotamente seus reposit�rios e oferecem ferramentas para a constru��o do software de maneira coloborativa. 
Assinale a alternativa que apresenta o comando utilizado para realizar a c�pia de um reposit�rio para a sua m�quina,
levando em considera��o:

Servidor: gitLab.com
Us�ario: rvenson
Reposit�rio: prova01

ALTERNATIVA:

D)git clone https: //gitlab.com/rvenson/prova01

JUSTIFICATIVA:

 Este comando clona um reposit�rio j� existente do gitlab.
 

